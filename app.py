import time

from flask import Flask, send_from_directory
from flask_cors import CORS

app = Flask(__name__)
CORS(app)

@app.route('/api/swagger.json')
def host():
  return send_from_directory('./', 'swagger.json')

@app.route('/api/swagger-without-host.json')
def nohost():
  return send_from_directory('./', 'swagger-without-host.json')
