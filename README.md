# **[DO NOT USE THIS PROJECT](https://about.gitlab.com/handbook/security/#other-servicesdevices)**

> Do not use tools designed to circumvent network firewalls for the purpose of exposing your laptop to the public Internet. An example of this would be using ngrok to generate a public URL for accessing a local development environment. Our core product offers remote code execution as a feature. Other applications we test often expose similar functionality via the relaxed nature of development environments. Running these on a laptop exposed to the Internet would essentially provide a back-door for remote attackers to abuse. This could result in the complete compromise of your home network and all business and personal accounts that have been accessed from your machine. Our Acceptable Use Policy prohibits circumventing the security of any computer owned by GitLab, and using ngrok in this manner is an example of circumventing our documented firewall requirements.

## Manual QA

This project defines an API with two `api` definitions (`swagger.json` and
`swagger-without-host.json`). The purpose is to determine whether or not
a `dast-api` scan with `DAST_API_TARGET_URL` omitted will scan the desired
`api`.

### Requirements

- `docker`
- `docker-compose`
- `ngrok`

### Step by Step

- Start services `docker-compose up`
- Make local server available to the internet `ngrok http 8002`
- Set `DAST_API_OPENAPI` to the desired `api` defintion e.g. `http://{ngrok-url}/api/swagger.json`
- Check scan output

## Outcomes

### `swagger.json`

[Appears](https://gitlab.com/philipcunningham/manual-qa-dast_api_openapi-only-behaviour/-/jobs/3134085452)
to override `host` in the definition, using the `ngrok` url.

```
$ /peach/analyzer-dast-api
04:55:23 [INF] API Security: Gitlab API Security
04:55:23 [INF] API Security: -------------------
04:55:23 [INF] API Security:
04:55:23 [INF] API Security: version: 2.0.69
04:55:23 [INF] API Security: api: http://127.0.0.1:5000
04:55:23 [INF] API Security: config: /peach/configs/gitlab-dast-api-config.yml
04:55:23 [INF] API Security: openapi: https://fce4-2001-8003-4db6-b601-4436-3f5f-75a6-39bd.ngrok.io/api/swagger.json
04:55:23 [INF] API Security: profile: Quick
04:55:23 [INF] API Security: project: philipcunningham/manual-qa-dast_api_openapi-only-behaviour
04:55:23 [INF] API Security: security report: gl-dast-api-report.json
04:55:23 [INF] API Security: security report asset path: gl-assets
04:55:23 [INF] API Security: ci_project_url: https://gitlab.com/philipcunningham/manual-qa-dast_api_openapi-only-behaviour
04:55:23 [INF] API Security: ci_job_id: 3134085452
04:55:23 [INF] API Security: service_start_timeout: 300
04:55:23 [INF] API Security: target_url: None
04:55:23 [INF] API Security: timeout: 30
04:55:23 [INF] API Security: verbose: False
04:55:23 [INF] API Security:
04:55:23 [INF] API Security: Waiting for API Security (http://127.0.0.1:5000) to become available...
04:55:23 [INF] API Security: Backing off 0.7 seconds afters 1 tries
04:55:23 [INF] API Security: Backing off 1.0 seconds afters 2 tries
04:55:52 [INF] API Security:
04:55:52 [INF] API Security: Loaded 20 operations from: https://fce4-2001-8003-4db6-b601-4436-3f5f-75a6-39bd.ngrok.io/api/swagger.json
04:55:52 [INF] API Security:
04:55:52 [INF] API Security: Testing operation [1/20]: 'POST https://fce4-2001-8003-4db6-b601-4436-3f5f-75a6-39bd.ngrok.io/v2/pet/64/uploadImage'.
04:55:52 [INF] API Security:  - Parameters: (Headers: 5, Query: 0, Body: 3)
04:55:52 [INF] API Security:  - Request body size: 363 Bytes (363 bytes)
04:55:52 [INF] API Security:
```

### `swagger-without-host.json`

[Appears](https://gitlab.com/philipcunningham/manual-qa-dast_api_openapi-only-behaviour/-/jobs/3134102800)
use the `ngrok` url.

```
 /peach/analyzer-dast-api
05:02:23 [INF] API Security: Gitlab API Security
05:02:23 [INF] API Security: -------------------
05:02:23 [INF] API Security:
05:02:23 [INF] API Security: version: 2.0.69
05:02:23 [INF] API Security: api: http://127.0.0.1:5000
05:02:23 [INF] API Security: config: /peach/configs/gitlab-dast-api-config.yml
05:02:23 [INF] API Security: openapi: https://49fa-2001-8003-4db6-b601-4436-3f5f-75a6-39bd.ngrok.io/api/swagger-without-host.json
05:02:23 [INF] API Security: profile: Quick
05:02:23 [INF] API Security: project: philipcunningham/manual-qa-dast_api_openapi-only-behaviour
05:02:23 [INF] API Security: security report: gl-dast-api-report.json
05:02:23 [INF] API Security: security report asset path: gl-assets
05:02:23 [INF] API Security: ci_project_url: https://gitlab.com/philipcunningham/manual-qa-dast_api_openapi-only-behaviour
05:02:23 [INF] API Security: ci_job_id: 3134102800
05:02:23 [INF] API Security: service_start_timeout: 300
05:02:23 [INF] API Security: target_url: None
05:02:23 [INF] API Security: timeout: 30
05:02:23 [INF] API Security: verbose: False
05:02:23 [INF] API Security:
05:02:23 [INF] API Security: Waiting for API Security (http://127.0.0.1:5000) to become available...
05:02:23 [INF] API Security: Backing off 0.9 seconds afters 1 tries
05:02:24 [INF] API Security: Backing off 0.4 seconds afters 2 tries
05:02:24 [INF] API Security: Backing off 0.7 seconds afters 3 tries
05:02:51 [INF] API Security:
05:02:51 [INF] API Security: Loaded 20 operations from: https://49fa-2001-8003-4db6-b601-4436-3f5f-75a6-39bd.ngrok.io/api/swagger-without-host.json
05:02:51 [INF] API Security:
05:02:51 [INF] API Security: Testing operation [1/20]: 'POST https://49fa-2001-8003-4db6-b601-4436-3f5f-75a6-39bd.ngrok.io/v2/pet/64/uploadImage'.
05:02:51 [INF] API Security:  - Parameters: (Headers: 5, Query: 0, Body: 3)
05:02:51 [INF] API Security:  - Request body size: 363 Bytes (363 bytes)
05:02:51 [INF] API Security:
```

### Notes

I cancelled both scans early, once it became obvious they were being accessed by the analyzer.

### Summary

It appears to use the host of where the `api` definition is located.
